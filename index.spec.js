require('jasmine');
var add = require('./index.js');
//test Suite
describe("Mon exemple de test NodeJS", function() {
    //test Case
    it("should return 2 when adding 1+1", function() {
      let actual = add(5,5);
      let expected =10;
      expect(actual).toEqual(expected); 
    });
  });